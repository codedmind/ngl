import "libraries/colours", "libraries/opthandler"
import os, osproc, json, strutils, sequtils, tables, strformat
import parsetoml, rdstdin
let VERSION = "0.0.8"

let root = getHomeDir()&".ngl.d/"
let confs = root&"configs/"
let repo = root&"repo/"
let confpath = confs&"data.json"
let opt = newOptHandler()
let conf = json.parseFile(confs&"data.json")
var usingdir = "default"
if conf.hasKey("internal") and conf["internal"]{"using"}.getStr != "": usingdir = conf["internal"]{"using"}.getStr
var data = root&usingdir&"/"
let scripts = root&"scripts/"

proc figlet*(s: string) = discard execShellCmd("figlet -k -c "&s)

proc setv*(group, key, value: string) =
    if not conf.hasKey(group): conf[group] = newJObject()
    conf[group][key] = value.newJString
    confpath.writeFile(conf.pretty)
    
proc getv*(group, key, value: string = ""): string =
    if conf.hasKey(group) and conf[group].hasKey(key): return conf[group][key].getStr
    return value

proc inPath*(path:string): bool =
    var pth = path
    if not pth.endsWith("/"): pth = pth&"/"
    for pim in getEnv("PATH").split(":"):
        var p = pim
        if not p.endsWith("/"): p = p&"/"
        if pth == p: return true
    return false
    
proc yes*(question: string): bool =
  echo question, " (y/n)"
  while true:
    case readLine(stdin)
    of "y", "Y", "yes", "Yes": return true
    of "n", "N", "no", "No": return false
    else: echo "Please be clear: yes or no"
    
proc executeNgl(line: string, secondFail: bool = false) =
    echo "Executing "&line
    let errC = execCmd(line)
    case errC
    of 0: echo "OK"
    of 1: echo "Application error!"
    of 126:
        if secondFail:
            echo "Can't correct this problem."
            return
        echo "Permissions error, correcting..."
        discard execCmd("chmod u+x "&line)
        echo "Trying again."
        executeNgl line, true
    else: echo fmt"Uncaught exit code {errC}"

proc executeRepoFile(name, args:string) =
    let r = parsetoml.parseFile(repo&"repo.toml")
    if not r.hasKey(name): echo "Not found..."; return
        
    let binary = r[name]{"binary"}.getStr("")
    let archive = r[name]{"archive"}.getStr("")
    let extractCommand = r[name]{"extractCommand"}.getStr("tar")
    let extractParams = r[name]{"extractParams"}.getStr("-xzf")
    let url = r[name]{"url"}.getStr("")
    let prerunner = r[name]{"pre_run"}.getStr("")
    let preexecute = r[name]{"pre_execute"}.getStr("")
    let runner = r[name]{"run"}.getStr("")
    let execute = r[name]{"execute"}.getStr("")
    let description = r[name]{"description"}.getStr("")
    let group = r[name]{"group"}.getStr("")
    let scriptExec = r[name]{"scriptExec"}.getStr("sh")
    #let scope = r[name]{"scope"}.getStr("ngl")
    let addPath = r[name]{"addPath"}.getStr("")
    var good = true
    var errc = 0
    
    # Download
    #let ftxt = ""
    if prerunner != "" and yes("====================\n"&readFile(getHomeDir()&"/.ngl.d/repo/.scripts/"&prerunner)&"\n====================\nRun this file?"):
            echo "Running init scripts...";
            discard execCmd(fmt"{scriptExec} ~/.ngl.d/repo/.scripts/{prerunner} {args}")
    
    if preexecute != "" and yes(fmt"`{preexecute}`\nRun this command?"): echo "Running init scripts...";discard execCmd(preexecute)
    
    if url != "":
        if fileExists(getHomeDir()&".ngl.d/repo/"&archive): echo "Skipping download, archive in cache."; errc = 0
        else: echo fmt"Downloading from {url}"; errc = execCmd(fmt"wget -P ~/.ngl.d/repo/ {url}")
        if errc == 0:
            echo "OK"
            #Extract
            if archive != "":
                echo "Extracting..."
                discard execCmd(fmt"{extractCommand} -C ~/.ngl.d/repo/ {extractParams} ~/.ngl.d/repo/{archive}")
                if binary != "":
                    echo "Creating link..."
                    discard execCmd(fmt"rm {data}{name}")
                    discard execCmd(fmt"ln -s {root}repo/{binary} {data}{name}")
      
        else:
            echo fmt"Error in download, aborting. {errc}"
            good = false
            
    # Finalize
    if good:
        if runner != "" and yes("====================\n"&readFile(getHomeDir()&"/.ngl.d/repo/.scripts/"&runner)&"\n====================\nRun this file?"):
            echo "Running scripts...";
            discard execCmd(fmt"{scriptExec} ~/.ngl.d/repo/.scripts/{runner} {args}")
        if execute != "" and yes(fmt"`{execute}`\nRun this command?"): echo "Running init scripts...";discard execCmd(execute)
   
        if description != "":
            setv name, "description", description
            echo fmt"UPDATE: description for {name} => {description}"
            setv name, "group", group
            echo fmt"UPDATE: group for {name} => {group}"
            
        if addPath != "" and not inPath(addPath):
            if yes("Add "&addPath&" to PATH?"):
                discard execCmd(fmt"cp {getHomeDir()}.bashrc {getHomeDir()}.bashrc.1")
                var txt = readFile(getHomeDir()&".bashrc")
                txt = txt&"\nexport PATH=$PATH:"&addPath
                writeFile(getHomeDir()&".bashrc", txt)
      
proc launchApp(c:string) =
    var options = newSeq[string]()
    let tolaunch = c.split(" ")[0]
    let args = c.split(" ")[1..^1]

    for kind, path in walkDir(data):
        if (kind == pcFile or kind == pcLinkToFile) and not path.endsWith("~"):
            var p = path.split("/")
            let filename = p.pop
            if filename.contains("."):
                let app = filename.split(".")[0..^2].join(".")
                let ext = filename.split(".")[^1]
                if app.toLower.starts_with tolaunch.toLower:
                    if ext.toLower == "desktop" or ext.toLower == "appimage": options.add(path&" "&args.join(" "))
                    else:
                        if conf.hasKey(ext.toLower): options.add(conf[ext]["launcher"].getStr&" "&path&" "&args.join(" "))
                        else: echo "Unknown handler for this filetype."
            else:
                if filename.toLower.starts_with tolaunch.toLower: options.add(path&" "&args.join(" "))

    if options.len == 0: echo "No results found."
    elif options.len == 1: executeNgl options[0]
    else:
        echo "Ambiguous choice"
        for i in 0..options.len-1: echo fmt"#{i}: {options[i]}"
        let resp = readLineFromStdin("Enter selection # > ")
        executeNgl options[parseInt(resp)]

proc switchDataDir(d:string) =
    let blocked = ["repo", "configs", "libraries", "scripts", "src"]
    if not dirExists(fmt"{root}{d}"): echo "Directory not found."; return
    if blocked.contains(d): echo "Not allowed."; return
        
    var dd = readFile(fmt"{root}data_dirs").split("\n")
    echo fmt"Data Directories: {dd}"
    if not dd.contains(d):
        echo "Not a recognized data directory, add it?"
        if readLineFromStdin("(y/n) > ") == "y":
            dd.add d
            writeFile(fmt"{root}data_dirs", dd.join("\n"))
        else:
            return
    data = root&d&"/"
    setv "internal", "using", d
    echo "Data directory switches to "&d

proc main(cmdin:string) =
    case cmdin
    of "forget":
        if opt.hasFlag("h"): echo "forget <data dir name>\nRemoves a remembered data dir."; return
        let d = opt.command(1)
        var dd = readFile(fmt"{root}data_dirs").split("\n")
        dd.keepIf(proc(x: string): bool = x != d)
        writeFile(fmt"{root}data_dirs", dd.join("\n"))
    of "data":
        if opt.hasFlag("h"): echo "data\nShows remembered data dirs."; return
        var dd = readFile(fmt"{root}data_dirs").split("\n")
        echo fmt"Currently using: {data.blue}"
        for diri in dd: 
            echo fmt" - {diri.blue}"
    of "use":
        switchDataDir opt.command(1, "default")
    #[of "github", "gh":
        if opt.hasFlag("h"): echo "github/gh <owner>/<repo>\nDownloads release from git repo."; return
        if "/" notin opt.command(1):
            echo "Format required: owner/repo"
            return

        let base_git = fmt"https://api.github.com/repos/{opt.command(1)}/releases"
    ]#
    of "download", "dl":
        if opt.hasFlag("h"): echo "download/dl <url>\nDownloads file in to applications directory."; return
        let url = opt.command(1)
        let errc = execCmd(fmt"wget -P {data} {url}")
        if errc == 0:
            echo "OK"
        else:
            echo fmt"Problem downloading, error {errc}"
            
    of "install", "i":
        if opt.hasFlag("h"): echo "install/i <name>\nRuns install script defined in repo.toml."; return
        executeRepoFile(opt.command(1), opt.commands[2..^1].join(" "))
        
    of "remove", "rm", "delete", "del":
        if opt.hasFlag("h"): echo "remove, rm, delete, del <app>\nDeletes app file from data."; return
        discard execCmd(fmt"rm {data}{opt.command(1)}")
        
    of "move", "mv":
        if opt.hasFlag("h"): echo "move/mv <name> <new data dir>\nMoves file to another directory in .ngl.d/ root."; return
        for kind, path in walkDir(data):
            if (kind == pcFile or kind == pcLinkToFile) and not path.endsWith("~") and opt.command(1) in path:
                var p = path.split("/")
                let filename = p.pop
                echo filename
                if yes(fmt"Move {filename} to {opt.command(2)}?"):
                    discard execCmd(fmt"mv {data}{filename} {root}{opt.command(2)}/{opt.command(1)}")
                    break
        
    of "rename":
        if opt.hasFlag("h"): echo "rename <name> <new name>\nChanges binary name."; return
        #echo fmt"Renaming {opt.command(1)} to {opt.command(2)}"
        for kind, path in walkDir(data):
            if (kind == pcFile or kind == pcLinkToFile) and not path.endsWith("~") and opt.command(1) in path:
                var p = path.split("/")
                let filename = p.pop
                echo filename
                if yes(fmt"Rename {filename} to {opt.command(2)}?"):
                    discard execCmd(fmt"mv {data}{filename} {data}{opt.command(2)}")
                    break

    of "config", "cfg":
        if opt.hasFlag("h"): echo "config/cfg\nOutputs global config."; return
        for k, v in conf.pairs:
            echo " === "&k.green&" ==="
            for key, val in v.pairs:
                echo fmt"{key.blue} = {val.getStr.blue}"

    of "set":
        if opt.hasFlag("h"): echo "set <group> <key> <value>\nSets a global config value."; return
        if opt.commands.len < 4: echo red"Bad format. Looking for: group key value"; return
        let group = opt.command(1, "")
        let key = opt.command(2, "")
        let value = opt.command(3, "")
        setv group, key, value

    of "get":
        if opt.hasFlag("h"): echo "get <group> <key>\nGets a global config value."; return
        if opt.commands.len < 3: echo red"Bad format. Looking for: group key"; return
        let group = opt.command(1, "")
        let key = opt.command(2, "")
        if conf.hasKey(group) and conf[group].hasKey(key): echo fmt"{group.blue}.{key.blue} = {conf[group][key].getStr.blue}"
    
    of "run:", "r:":
        if opt.hasFlag("h"): echo "run: <data dir> <command>\nLaunches application from data directory. First term is the data directory the app is in, second is application name, any text following is sent as arguments."; return
        echo commandLineParams()
        let newdir = commandLineParams()[1]
        var newcmd = commandLineParams()[2..^1].join(" ")
        switchDataDir newdir
        launchApp newcmd

    of "run", "r":
        if opt.hasFlag("h"): echo "run <command>\nLaunches application from data directory. First term is the application name, any text following is sent as arguments."; return
        launchApp commandLineParams()[1..^1].join(" ")

    of "list", "ls":
        if opt.hasFlag("h"): echo "list/ls\nLists current data directory."; return
        var outputs = initTable[string, seq[string]]()
        
        for kind, path in walkDir(data):
            if kind == pcLinkToFile:
                var p = path.split("/")
                let filename = p.pop
                var app = filename
                var appln = filename.blue&" (Link)"
                var grp = ""
                if conf.hasKey(app.toLower) and conf[app.toLower].hasKey("group"): grp = conf[app.toLower]["group"].getStr
                else:grp = "Other"
                if conf.hasKey(app.toLower) and conf[app.toLower].hasKey("description"): appln.add "\n   * "&conf[app.toLower]["description"].getStr
                if not outputs.hasKey(grp.toLower): outputs[grp.toLower] = newSeq[string]()
                outputs[grp.toLower].add appln
                
            if kind == pcFile and not path.endsWith("~"):
                var p = path.split("/")
                let filename = p.pop
                var app = filename
                var ext = ""
                var appln = filename.blue
                if filename.contains("."):
                    app = filename.split(".")[0..^2].join(".")
                    ext = filename.split(".")[^1]
                    if conf.hasKey(ext.toLower): appln = app.blue&" ("&conf[ext.toLower]["name"].getStr.toLower.blue&")"
                    else: appln = app.blue&" ("&ext.toLower.red&"?)"
                var grp = ""
                if conf.hasKey(app.toLower) and conf[app.toLower].hasKey("group"): grp = conf[app.toLower]["group"].getStr
                else:grp = "Other"
                if conf.hasKey(app.toLower) and conf[app.toLower].hasKey("description"): appln.add "\n   * "&conf[app.toLower]["description"].getStr
                if not outputs.hasKey(grp.toLower): outputs[grp.toLower] = newSeq[string]()
                outputs[grp.toLower].add appln
                
        for k, v in outputs:
            echo "=== "&k.green&" ==="
            for ent in v: echo " - "&ent
            
    of "link", "ln":
        if opt.hasFlag("h"): echo "link/ln <app> <alias>\nAdds a link to users system binary directory."; return
        discard execCmd fmt"rm {getHomeDir()}/.local/bin/{opt.command(2)}"
        discard execCmd fmt"ln -s {data}/{opt.command(1)} {getHomeDir()}/.local/bin/{opt.command(2)}"
    of "import":
        if opt.hasFlag("h"): echo "import <path to binary>\nImports a binary file to be indexed in NGL.\nTakes flag -r to use a raw command, and skip validation."; return
        if fileExists(opt.command(1)):
            let name = opt.command(1).split("/")[^1].split(".")[0]
            discard execCmd fmt"rm {data}{name}"
            discard execCmd fmt"ln -s {opt.command(1)} {data}{name}"
        else:
            echo "Executable not found."
    of "help", "h":
        "NGL".figlet
        echo fmt"Nim Generic Launcher {VERSION}"
        echo ""
        echo blue"run <app string>"
        echo blue"run: <data directory> <app string>"
        echo blue"download <url>"
        echo blue"install <repo key>"
        echo blue"import <alias> <path>"
        echo blue"move <app> <new dir>"
        echo blue"rename <name> <new>"
        echo blue"link <data directory application> <new alias>"
        echo blue"use [data directory]"
        echo blue"data"
        echo blue"forget <data directory>"
        echo blue"set <group> <key> <value>"
        echo blue"get <group> <key>"
        echo blue"config"
        for kind, path in walkDir(scripts):
            if kind == pcFile and not path.endsWith("~"):
                var p = path.split("/")
                let filename = p.pop
                if filename.contains ".": echo fmt"{filename.split('.')[0].yellow}"
                else: echo fmt"{filename.yellow}"
    else:
        let script = opt.command(0, "")
        if script == "":
            echo "What?"
            return
            
        for kind, path in walkDir(scripts):
            if kind == pcFile and not path.endsWith("~"):
                var p = path.split("/")
                let filename = p.pop
                var app = filename
                var ext = ""
                var launcher = ""
                if filename.contains("."):
                    app = filename.split(".")[0..^2].join(".")
                    if script == app:
                        ext = filename.split(".")[^1]
                        if conf.hasKey(ext.toLower): launcher = conf[ext.toLower]["launcher"].getStr.toLower
                        if launcher == "":
                            echo "Undefined launcher for this script type..."
                        else:
                            let ln = opt.commands[1..^1].join(" ")
                            if launcher == "ruby": # custom hook
                                discard execCmd fmt"{launcher} {root}ngl.rb {app} {ln}"
                            elif launcher == "python3": # custom hook
                                discard execCmd fmt"{launcher} {root}ngl.py {app} {ln}"
                            else:
                                discard execCmd fmt"{launcher} {path} {ln}"    
                else:
                    if script == filename:
                        let ln = opt.commands[1..^1].join(" ")
                        discard execCmd path&" "&ln
                        
                    
                    

main(opt.command(0, "help"))

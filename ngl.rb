$ROOTDIR = "#{Dir.home}/.ngl.d/"
$SCRIPTS = "#{$ROOTDIR}/scripts/"
$APPS = "#{$ROOTDIR}/default/"
$LOAD_PATH.unshift $ROOTDIR
$LOAD_PATH.unshift $SCRIPTS
require ARGV[0]

def main
  send "run_#{ARGV[0]}"
end

main

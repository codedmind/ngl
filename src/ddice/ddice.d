import std.stdio, std.algorithm, std.array, std.string, std.process, std.conv, tcm.colours, tcm.strplus;
import std.random, tcm.randplus, tcm.figlet;

import dice;

void main(string[] argv){
  if (argv.length == 1)
    rollDice("2d6").result.writeln;
  else
    rollDice(argv[1]).result.writeln;
}

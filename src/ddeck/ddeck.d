import std.stdio, std.algorithm, std.array, std.string, std.process, std.conv, tcm.colours, tcm.strplus;
import std.random, tcm.randplus, tcm.figlet, std.json, std.file, std.path;

import cards;

void main(string[] argv){
  if (argv.length == 1)  deckSim("standard");
  else deckSim(argv[1]);
}

void deckSim(string pack){
  auto clr = new Colours();
  //executeShell("clear").output.writeln;
  figlet("cards");
  auto deck = new Deck(pack);
  writeln("Commands: ["~clr.yellow~"draw (optional number)"~clr.reset~"] ["~clr.yellow~"hand"~clr.reset~"] ["~clr.yellow~"shuffle"~clr.reset~"] ["~clr.yellow~"reshuffle (optional new pack)"~clr.reset~"] ["~clr.yellow~"deck"~clr.reset~"] ["~clr.yellow~"packs"~clr.reset~"] ["~clr.yellow~"combine"~clr.reset~"] ["~clr.yellow~"clear"~clr.reset~"] "~clr.red~"(ctrl-c to exit)"~clr.reset~"");
  Card[] hand;
  Card[] discards;
  string line;
  write("> ");
  while((line = readln()) !is null){
    line = line.replace("\n", "");
    string com = line.split(" ")[0];
    string p;
    if(line.contains(" ")){
	p = line.split(" ")[1..$].join(" ");
    }
    switch(com){
    case "clear":
	executeShell("clear").output.writeln;
	figlet("cards");
	writeln("Commands: ["~clr.yellow~"draw (optional number)"~clr.reset~"] ["~clr.yellow~"hand"~clr.reset~"] ["~clr.yellow~"shuffle"~clr.reset~"] ["~clr.yellow~"reshuffle (optional new pack)"~clr.reset~"] ["~clr.yellow~"deck"~clr.reset~"] ["~clr.yellow~"packs"~clr.reset~"] ["~clr.yellow~"combine"~clr.reset~"] ["~clr.yellow~"clear"~clr.reset~"] "~clr.red~"(ctrl-c to exit)"~clr.reset~"");
	break;
    case "draw":
	int num = 1;
	if(p != ""){
	  num = p.to!int;
	}
	for(int i = 0;i<num;i++){
	  auto c = deck.draw();
	  writeln("Drew "~c.toString());
	  hand ~= c;
	}

	break;
    case "hand":
	foreach(c; hand){writeln(" "~c.toString()~" ");}
	break;
    case "packs":
	writeln(deck.validPacks.join(" | "));
	break;
    case "deck":
	foreach(c; deck.cards){writeln(" "~c.toString()~" ");}
	write("Packs: ");
	writeln(deck.packs.join(" | "));
	writeln("Cards: "~deck.cards.length.to!string);
	break;
    case "shuffle":
	deck.shuffle();
	break;
    case "reshuffle":
	hand = []; discards = [];
	if(p != "") deck.reshuffle(p); else deck.reshuffle(pack);
	break;
    case "combine":
	writeln(deck.validPacks.join(" | "));
	write("Add in which pack? > ");
	deck.generatePack(readln().strip);
	break;
    default:
	writeln("What?");
    }
    write("> ");
  }
}
void printHand(Card[] h, string fmsg){ write(fmsg); foreach(c;h){ write(c.toString.blue); } write("\n"); }
int calculateHand(Card[] h){int i; foreach(c;h){ i += c.value; } return i;}

import std.stdio, std.algorithm, std.array, std.string, std.process, std.conv, tcm.colours, tcm.strplus;
import std.random, tcm.randplus, tcm.figlet;

import cards;

void main(string[] argv){
  playBlackjack();
}
void printHand(Card[] h, string fmsg){ write(fmsg); foreach(c;h){ write(c.toString.blue); } write("\n"); }
int calculateHand(Card[] h){int i; foreach(c;h){ i += c.value; } return i;}

void playBlackjack(){
  executeShell("clear").output.writeln;
  figlet("blackjack");
  Card[] player;
  Card[] house;
  Card card;
  bool running = true;
  auto d = new Deck();
  d.jokers = 0;
  
  d.reshuffle();
  player ~= d.draw();
  player ~= d.draw();
  house ~= d.draw();
  house ~= d.draw();

  while(running){
    writeln("\n\n\n");
    printHand(player, "You have ");
    writeln("Value of "~calculateHand(player).to!string.blue);
    writeln("House has "~house[0].toString.red~" and a hidden card.");

    write("What do you do? [draw/hit | stand] > ");
    string prompt = readln().strip;
    switch(prompt){
    case "d", "dr", "dra", "draw", "h", "hi", "hit":
	card = d.draw();
	writeln("You drew "~card.toString.blue);
	player ~= card;
	writeln("Total value is now "~player.calculateHand.to!string);
	if(player.calculateHand > 21) {"Bust!".figlet; running=false;}
	if(player.calculateHand == 21) {"You win!".figlet; running=false;}
	break;

    case "s", "st", "sta", "stan", "stand":
	writeln("You stand.");
	break;

    default:
	writeln("What?");
	break;
    }

  }
	
}

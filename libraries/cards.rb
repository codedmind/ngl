require "json"

class Deck
  def initialize(deckfile)
    @data = JSON.parse(File.read(deckfile))
  end
end

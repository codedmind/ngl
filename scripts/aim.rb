require 'json'

def run_aim
    cfg = JSON.parse(File.read("#{Dir.home}/.ngl.d/configs/data.json"))
    using = cfg["internal"]["using"]
    puts "Installing appimages to /#{using}"
    path = "#{Dir.home}/Downloads/"

    Dir.foreach(path) do |filename|
    next if filename == '.' or filename == '..'
        filepath = path + filename

        if filename.downcase().end_with?(".appimage")
            puts "Moving #{filename}"
            `mv #{filepath} #{Dir.home}/.ngl.d/#{using}`
        end
    end 
end
